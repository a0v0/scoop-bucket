# aV's scoop apps

| Name                       | Install Command    | URL                                       | Version    | Type       |
|----------------------------|--------------------|-------------------------------------------|------------|------------|
| Internet Download Manager | `scoop install idm-pro`  | https://www.internetdownloadmanager.com    | v6.38.16   | Pro        |
| Driver Easy Pro            | `scoop install drivereasy-pro` | https://www.drivereasy.com                  | v5.6.15.34863 | Pro        |
| KMSPico                    | `scoop install kmspico` | https://www.getkmspico.com/                | v10.1.8    | Freemium   |
| MobaXTerm Pro              | `scoop install mobaxterm-pro` | https://mobaxterm.mobatek.net/              | v22.2      | Pro        |
| Navicat Pro                | `scoop install navicat-pro` | https://navicat.com/en                      | v16.0.7    | Pro        |
| WinToUsb Pro               | `scoop install wintousb-pro` | https://www.easyuefi.com/wintousb/          | v6.0       | Pro        |
| Inpaint Pro                | `scoop install inpaint-pro` | https://theinpaint.com/                     | v9.2       | Pro        |
Docker Desktop|`scoop install docker-desktop`|http://docker.com| nightly | Freemium
| Xmind| `scoop install xmind-pro`| http://xmind.app| v23.07.180256| Pro

## Other Useful Buckets
- [Shovel-Ash258](https://github.com/Ash258/Shovel-Ash258)
- [ScoopMaster](https://github.com/okibcn/ScoopMaster)
